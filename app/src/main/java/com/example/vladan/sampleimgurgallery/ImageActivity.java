package com.example.vladan.sampleimgurgallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageActivity extends AppCompatActivity {

    @BindView(R.id.big_image) ImageView bigImage;
    @BindView(R.id.description) TextView imageDescription;
    @BindView(R.id.title) TextView imageTitle;
    @BindView(R.id.up_votes) TextView ups;
    @BindView(R.id.down_votes) TextView downs;
    @BindView(R.id.score) TextView score;
    @BindString(R.string.up_votes) String upVotesPrefix;
    @BindString(R.string.down_votes) String downVotesPrefix;
    @BindString(R.string.score) String scorePrefix;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        imageTitle.setText(intent.getStringExtra("title"));
        imageDescription.setText(intent.getStringExtra("description"));
        ups.setText(String.format("%s %s", upVotesPrefix, intent.getStringExtra("upVotes")));
        downs.setText(String.format("%s %s", downVotesPrefix, intent.getStringExtra("downVotes")));
        score.setText(String.format("%s %s", scorePrefix, intent.getStringExtra("score")));
        Glide.with(this)
                .load(intent.getStringExtra("image"))
                .into(bigImage);
    }
}
