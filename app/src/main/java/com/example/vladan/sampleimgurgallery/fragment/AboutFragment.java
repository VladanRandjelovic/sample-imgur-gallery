package com.example.vladan.sampleimgurgallery.fragment;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vladan.sampleimgurgallery.R;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutFragment extends Fragment {

    @BindView(R.id.app_version) TextView appVersion;
    @BindString(R.string.app_version) String appVersionPrefix;

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, view);
        renderAppVersion();
        return view;
    }

    private void renderAppVersion() {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            int buildNumber = pInfo.versionCode;
            appVersion.setText(String.format("%s : %s - %d", appVersionPrefix, version, buildNumber));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}
