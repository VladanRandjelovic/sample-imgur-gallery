package com.example.vladan.sampleimgurgallery;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.vladan.sampleimgurgallery.fragment.AboutFragment;
import com.example.vladan.sampleimgurgallery.fragment.gallery.GalleryFragment;

import butterknife.BindView;

import static com.example.vladan.sampleimgurgallery.fragment.gallery.GalleryFragment.HOT_SECTION;
import static com.example.vladan.sampleimgurgallery.fragment.gallery.GalleryFragment.TOP_SECTION;
import static com.example.vladan.sampleimgurgallery.fragment.gallery.GalleryFragment.USER_SECTION;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedSection = null;
                switch (item.getItemId()) {
                    case R.id.navigation_hot:
                        selectedSection = GalleryFragment.newInstance(HOT_SECTION);
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.list_frame, selectedSection)
                                .commit();
                        break;
                    case R.id.navigation_top:
                        selectedSection = GalleryFragment.newInstance(TOP_SECTION);
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.list_frame, selectedSection)
                                .commit();
                        break;
                    case R.id.navigation_user:
                        selectedSection = GalleryFragment.newInstance(USER_SECTION);
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.list_frame, selectedSection)
                                .commit();

                        break;
                    case R.id.navigation_about:
                        selectedSection = AboutFragment.newInstance();
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.list_frame, selectedSection)
                                .commit();
                }
                return true;
            }
        });


        navigation.setItemIconTintList(null);

        if (savedInstanceState == null) {
            navigation.setSelectedItemId(R.id.navigation_hot);
        }
    }

}
