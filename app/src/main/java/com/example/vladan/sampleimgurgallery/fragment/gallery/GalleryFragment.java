package com.example.vladan.sampleimgurgallery.fragment.gallery;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.vladan.sampleimgurgallery.R;
import com.example.vladan.sampleimgurgallery.adapter.GalleryListAdapter;
import com.example.vladan.sampleimgurgallery.datamodel.GalleryImageDataModel;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static com.example.vladan.sampleimgurgallery.fragment.gallery.GallerySectionPresenter.EXTRA_TYPE;
import static com.example.vladan.sampleimgurgallery.fragment.gallery.GallerySectionPresenter.EXTRA_VIRAL;

public class GalleryFragment extends Fragment implements GallerySectionContract.View {

    public static final String EXTRA_LAYOUT_MANAGER_TYPE = "EXTRA_LAYOUT_MANAGER_TYPE";
    public static final int HOT_SECTION = 0;
    public static final int TOP_SECTION = 1;
    public static final int USER_SECTION = 2;

    private static final int GRID_LAYOUT = 3;
    private static final int LINEAR_LAYOUT = 4;
    private static final int STAGGERED_LAYOUT = 5;

    private GallerySectionContract.Presenter presenter;
    private GalleryListAdapter adapter;
    private int layoutManagerType;

    @BindView(R.id.gallery) RecyclerView gallery;
    @BindView(R.id.viral_images_toggle) CheckBox viralCheckbox;
    @BindView(R.id.sort_spinner) Spinner sortSpinner;
    @BindView(R.id.window_spinner) Spinner windowSpinner;
    @BindString(R.string.error_encountered) String errorPrefix;

    public static GalleryFragment newInstance(int type) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);
        showImages(null);

        Bundle bundle = savedInstanceState == null ? getArguments() : savedInstanceState;
        layoutManagerType = bundle.getInt(EXTRA_LAYOUT_MANAGER_TYPE, GRID_LAYOUT);

        if (layoutManagerType == GRID_LAYOUT) {
            setGridLayoutManager();
        } else if (layoutManagerType == STAGGERED_LAYOUT) {
            setStaggeredLayoutManager();
        } else {
            setLinearLayoutManager();
        }

        presenter = new GallerySectionPresenter();
        presenter.setView(this);

        int type = bundle.getInt(EXTRA_TYPE);
        if (type == USER_SECTION) {
            viralCheckbox.setVisibility(View.VISIBLE);
            sortSpinner.setVisibility(View.VISIBLE);
            setSortSpinnerAdapter();
            setOnSortTypeSelected();
        } else if (type == TOP_SECTION) {
            windowSpinner.setVisibility(View.VISIBLE);
            setWidowSpinnerAdapter();
            setOnWindowTypeSelected();
        }

        boolean showViral = bundle.getBoolean(EXTRA_VIRAL, true);
        presenter.setData(type, showViral);
        return view;
    }
    public void setWidowSpinnerAdapter() {
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.window_spinner_items, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        windowSpinner.setAdapter(spinnerAdapter);
    }

    public void setSortSpinnerAdapter() {
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.sort_spinner_items, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(spinnerAdapter);
    }

    public void setOnSortTypeSelected() {
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedSort = parent.getItemAtPosition(position).toString();
                if (selectedSort.equals("Top")) {
                    presenter.getSortTop();
                } else if (selectedSort.equals("Rising")) {
                    presenter.getSortRising();
                } else if (selectedSort.equals("Time")) {
                    presenter.getSortTime();
                } else {
                    presenter.getSortViral();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setOnWindowTypeSelected() {
        windowSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedWindow = parent.getItemAtPosition(position).toString();
                if (selectedWindow.equals("Week")) {
                    presenter.getWindowWeek();
                } else if (selectedWindow.equals("Month")) {
                    presenter.getWindowMonth();
                } else if (selectedWindow.equals("Year")) {
                    presenter.getWindowYear();
                } else if (selectedWindow.equals("All")) {
                    presenter.getWindowAll();
                } else {
                    presenter.getWindowDay();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void setGridLayoutManager() {
        int nubmerOfColumns = 3;
        gallery.setLayoutManager(new GridLayoutManager(getActivity(), nubmerOfColumns));
        adapter.setRowLayout(R.layout.item_gallery_image);
        this.layoutManagerType = GRID_LAYOUT;
    }

    private void setLinearLayoutManager() {
        gallery.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.setRowLayout(R.layout.item_gallery_image_linear);
        this.layoutManagerType = LINEAR_LAYOUT;
    }

    private void setStaggeredLayoutManager() {
        int nubmerOfColumns = 3;
        gallery.setLayoutManager(new StaggeredGridLayoutManager(nubmerOfColumns, StaggeredGridLayoutManager.VERTICAL));
        adapter.setRowLayout(R.layout.item_gallery_image_staggered);
        this.layoutManagerType = STAGGERED_LAYOUT;
    }

    @Override
    public void showImages(List<GalleryImageDataModel> galleryImages) {
       if (adapter == null) {
            adapter = new GalleryListAdapter(galleryImages);
            gallery.setAdapter(adapter);
        } else {
           adapter.setGalleryImages(galleryImages);
       }
    }

    @Override
    public void showNoInternetConnection() {
        Toast.makeText(getActivity(), R.string.no_internet_connection_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(getActivity(), String.format("%s %s", errorPrefix, error), Toast.LENGTH_LONG).show();
    }

    @OnCheckedChanged(R.id.viral_images_toggle)
    protected void onShowViralChanged(boolean checked) {
        presenter.showViralImages(checked);
    }

    @OnClick(R.id.grid)
    void onGridClicked() {
        setGridLayoutManager();
    }

    @OnClick(R.id.list_view)
    void onListClicked() {
        setLinearLayoutManager();
    }

    @OnClick(R.id.staggered_grid_view)
    void onStaggeredClicked() {
        setStaggeredLayoutManager();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.saveInstanceState(outState);
        outState.putInt(EXTRA_LAYOUT_MANAGER_TYPE, layoutManagerType);
    }
}
