package com.example.vladan.sampleimgurgallery.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.vladan.sampleimgurgallery.ImageActivity;
import com.example.vladan.sampleimgurgallery.R;
import com.example.vladan.sampleimgurgallery.datamodel.GalleryImageDataModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryListAdapter extends RecyclerView.Adapter<GalleryListAdapter.ImageViewHolder> {

    private List<GalleryImageDataModel> galleryImages;
    private int rowLayout;
    public static class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.gallery_image) ImageView galleryImage;
        @BindView(R.id.image_description) TextView imageDescription;

        private ImageClickListener imageClickListener;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setImageClickListener(ImageClickListener imageClickListener) {
            this.imageClickListener = imageClickListener;
        }

        @Override
        public void onClick(View view) {
            imageClickListener.onClick(view);
        }
    }

    public GalleryListAdapter(List<GalleryImageDataModel> galleryImages) {
        this.galleryImages = galleryImages;
    }

    public void setGalleryImages(List<GalleryImageDataModel> galleryImages) {
        this.galleryImages = galleryImages;
        notifyDataSetChanged();
    }

    public void setRowLayout(int rowLayout) {
        this.rowLayout = rowLayout;
        notifyDataSetChanged();
    }

    @Override
    public GalleryListAdapter.ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new GalleryListAdapter.ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GalleryListAdapter.ImageViewHolder holder,
                                 final int position) {
        holder.imageDescription.setText(galleryImages.get(position).getDescription());
        Glide.with(holder.itemView)
                .load(galleryImages.get(position).getImage())
                .into(holder.galleryImage);
        holder.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(View view) {
                Activity context = (Activity) view.getContext();
                Intent intent = new Intent(context, ImageActivity.class );
                intent.putExtra("image", galleryImages.get(position).getImage());
                intent.putExtra("title", galleryImages.get(position).getTitle());
                intent.putExtra("upVotes", String.format("%d", galleryImages.get(position).getUpVotes()));
                intent.putExtra("downVotes", String.format("%d", galleryImages.get(position).getDownVotes()));
                intent.putExtra("description", galleryImages.get(position).getDescription());
                intent.putExtra("score", String.format("%d", galleryImages.get(position).getScore()));
                context.startActivityForResult(intent, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (galleryImages == null) {
            return 0;
        }
        return galleryImages.size();
    }
}
